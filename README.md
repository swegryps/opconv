# Disclaimer
opconv is a simple bash-script to convert audio-files in any format ffmpeg can handle to .opus.

opconv is Free Software and all of the source code is licenced under
the GPL, version 3 or higher (at your option).

# Installation
To install opconv copy it to any place defined in PATH. If you are a single user you could put it in ~/bin f.ex.

As named in the disclaimer opconv depends on ffmpeg.

# Usage

	opconv [-d] FILE1 FILE2 ...

By now -d is the only available option. It deletes the original file after conversion. Use it with caution.

Errormessages will be written in the logifile opconv.log in the map where you startet opconv.

# Contact

You can contact me for questions and suggestions:

mail: gryps@fripost.org
XMPP: folky@jabber.se
gnusocial: gryps@status.vinilox.eu
